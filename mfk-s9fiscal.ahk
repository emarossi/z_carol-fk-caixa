﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance
#NoTrayIcon
DetectHiddenText, On
DetectHiddenWindows, On

LiberadoAdm := False
SenhaLiberacao = 192168
HoraLiberado = 0
TempoLiberado = 900 ; 15 minutos

Gui, Font,s20
Gui, Add, Text,, LIBERACAO
Gui, Add, Text,, Senha administrador:
Gui, Add, Edit, Password w300 vSenha
Gui, Add, Button, w300 default, OK


SetTimer, VerificaLibera, 3000
!^l::Gosub, LogoutF
^!s::MsgBox, CarolSupervisor Versao 1.6

return


LogoutF:
Process, Close, S9_Fiscal.exe
Run, taskkill /f /im S9_Fiscal.exe
Sleep 300
LiberadoAdm := False
return

ProcessExist(Name){
	Process,Exist,%Name%
	return ErrorLevel
}

VerificaLibera:
    if(LiberadoAdm = True and A_Now > HoraLiberado + TempoLiberado) {
       LiberadoAdm := False  
    }
    if (!LiberadoAdm) 
    {
       if ProcessExist("S9_Fiscal.exe") 
       { 
          Process, Close, S9_Fiscal.exe
          Run, taskkill /f /im S9_Fiscal.exe
          Gui, Show,, LIBERACAO
       } 
       
    } 
return


ButtonOK:
Gui, Submit, NoHide
        if SenhaLiberacao = %Senha%
        {
	   LiberadoAdm := True
           HoraLiberado := A_Now
           GuiControl,,Senha,
           Gui, Hide,
        }
        else {
           MsgBox, Senha Incorreta
        }
return